job('Test-King') {
    scm {
      filesystem {
          path('/opt/jenkins-folder/test/oreilly-gradle-book-examples/web-hello-world')
          clearWorkspace(false)
          copyHidden(false)
        }
    }
    steps {
        gradle{
        gradleName ('Gradle 3.2.1')
        tasks ('test')
        useWrapper (false)
        rootBuildScriptDir ('/opt/jenkins-folder/test/oreilly-gradle-book-examples/web-hello-world')
        }
    }
}
