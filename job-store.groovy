job('Store-King') {
    triggers {
        upstream('Build-King')
    }
    steps {
        copyArtifacts('Build-King') {
            includePatterns('web-hello-world/build/libs/web-hello-world.war')
            targetDirectory('/opt/jenkins-folder/${BUILD_TIMESTAMP}')
            buildSelector {
                workspace()
            }
        }
        copyArtifacts('Build-King') {
            includePatterns('web-hello-world/build/libs/web-hello-world.war')
            targetDirectory('/var/lib/jenkins/workspace/Store-King')
            buildSelector {
                workspace()
            }
        }
    }
    publishers {
        archiveArtifacts {
            pattern('web-hello-world/build/libs/*.war')
            onlyIfSuccessful()
        }
    }

}
