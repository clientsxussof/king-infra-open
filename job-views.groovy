listView('King') {
    description('View for project King')
    columns {
        status()
        weather()
        name()
        lastSuccess()
        lastFailure()
        lastDuration()
        buildButton()
    }
    jobs {
        name('Build-King')
        name('Store-King')
    }
}
