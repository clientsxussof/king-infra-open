job('Build-King') {
    scm {
        git('https://github.com/gradle/oreilly-gradle-book-examples.git')
    }
    steps {
        gradle{
        gradleName ('Gradle 3.2.1')
        tasks ('build')
        useWrapper (false)
        rootBuildScriptDir ('web-hello-world')
        }
    }
}
