#!/usr/bin/env python

import json, yaml

inputfile = 'inventory'

with open("./inventories/" + inputfile + ".yml", 'r') as stream:
   try:
       print(json.dumps(yaml.load(stream),  sort_keys=True, indent=4, separators=(',', ': ')))
   except yaml.YAMLError as exc:
       print(exc)
